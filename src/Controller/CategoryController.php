<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Error;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/category', name: 'app_category_')]
class CategoryController extends BaseController
{
    #[Route(path: '', name: 'getAll', methods: ['GET'])]
    public function getAll(CategoryRepository $categoryRepository): JsonResponse
    {
        $categories = $categoryRepository->findAll();

        $categoriesResponse = [];

        foreach ($categories as $category) {
            $categoriesResponse[] = $this->formatResponseCategory($category);
        }

        return $this->json([
            'categories' => $categoriesResponse
        ]);
    }

    #[Route(path: '/{id}', name: 'get', methods: ['GET'])]
    public function get(Request $request, CategoryRepository $categoryRepository): JsonResponse
    {
        try {
            $category = $categoryRepository->findOneById($this->getIdFromRoute($request));

            return $this->returnCategory($category);
        } catch (Error $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '', name: 'post', methods: ['POST'])]
    public function post(Request $request, CategoryRepository $categoryRepository): JsonResponse
    {
        try {
            $category = new Category();
            $category->setName($request->request->get('name'));

            $categoryRepository->add($category, true);

            return $this->returnCategory($category);
        } catch (Error|UniqueConstraintViolationException $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '/{id}', name: 'patch', methods: ['PATCH'])]
    public function patch(Request $request, CategoryRepository $categoryRepository): JsonResponse
    {
        try {
            $category = $categoryRepository->findOneById($this->getIdFromRoute($request));

            $name = $request->request->get('name');
            $categoryRepository->patch($category, $name, true);

            return $this->returnCategory($category);
        } catch (\Error $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(Request $request, CategoryRepository $categoryRepository): Response | JsonResponse
    {
        try {
            $category = $categoryRepository->findOneById($this->getIdFromRoute($request));

            $categoryRepository->delete($category, true);

            return $this->returnNoContent();
        } catch (\Error $error) {
            return $this->returnError($error);
        }
    }

    private function returnCategory(Category $category): JsonResponse
    {
        return $this->json([
            'category' => $this->formatResponseCategory($category)
        ]);
    }
}
