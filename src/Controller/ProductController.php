<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Error;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/product', name: 'app_product_')]
class ProductController extends BaseController
{

    #[Route(path: '', name: 'getAll', methods: ['GET'])]
    public function getAll(ProductRepository $productRepository): JsonResponse
    {
        $products = $productRepository->findAll();

        $productsResponse = [];

        foreach ($products as $product) {
            $productsResponse[] = $this->formatResponseProduct($product);
        }

        return $this->json([
            'products' => $productsResponse
        ]);
    }

    #[Route(path: '/{id}', name: 'get', methods: ['GET'])]
    public function get(Request $request, ProductRepository $productRepository): JsonResponse
    {
        try {
            $product = $productRepository->findOneById($this->getIdFromRoute($request));

            return $this->returnProduct($product);
        } catch (Error $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '', name: 'post', methods: ['POST'])]
    public function post(Request $request, ProductRepository $productRepository, CategoryRepository $categoryRepository): JsonResponse
    {
        try {
            $category = $categoryRepository->findOneById($request->request->get('idCategory'));

            $product = new Product();
            $product->setName($request->request->get('name'));
            $product->setCategory($category);

            $productRepository->add($product, true);

            return $this->returnProduct($product);
        } catch (Error|UniqueConstraintViolationException $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '/{id}', name: 'patch', methods: ['PATCH'])]
    public function patch(Request $request, ProductRepository $productRepository, CategoryRepository $categoryRepository): JsonResponse
    {
        try {
            $product = $productRepository->findOneById($this->getIdFromRoute($request));
            $category = null;

            $idCategory = $request->request->get('idCategory');

            if ($idCategory) {
                $category = $categoryRepository->findOneById($idCategory);
            }

            $name = $request->request->get('name');
            $productRepository->patch($product, $name, $category, true);

            return $this->returnProduct($product);
        } catch (\Error $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(Request $request, ProductRepository $productRepository): Response | JsonResponse
    {
        try {
            $product = $productRepository->findOneById($this->getIdFromRoute($request));

            $productRepository->delete($product, true);

            return $this->returnNoContent();
        } catch (\Error $error) {
            return $this->returnError($error);
        }
    }

    private function returnProduct(Product $product): JsonResponse
    {
        return $this->json([
            'product' => $this->formatResponseProduct($product)
        ]);
    }
}
