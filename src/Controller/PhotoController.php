<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Repository\PhotoRepository;
use App\Repository\ProductRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Error;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/photo', name: 'app_photo_')]
class PhotoController extends BaseController
{

    #[Route(path: '', name: 'getAll', methods: ['GET'])]
    public function getAll(PhotoRepository $photoRepository): JsonResponse
    {
        $photos = $photoRepository->findAll();

        $photosResponse = [];

        foreach ($photos as $photo) {
            $photosResponse[] = $this->formatResponsePhoto($photo);
        }

        return $this->json([
            'photos' => $photosResponse
        ]);
    }

    #[Route(path: '/{id}', name: 'get', methods: ['GET'])]
    public function get(Request $request, PhotoRepository $photoRepository): JsonResponse
    {
        try {
            $photo = $photoRepository->findOneById($this->getIdFromRoute($request));

            return $this->returnPhoto($photo);
        } catch (Error $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '', name: 'post', methods: ['POST'])]
    public function post(Request $request, PhotoRepository $photoRepository, ProductRepository $productRepository): JsonResponse
    {
        try {
            $limit = $this->getParameter('allowedCountPhotosOnProduct');
            $product = $productRepository->findOneById($request->request->get('idProduct'));

            $photo = new Photo();
            $photo->setFilename($request->request->get('filename'));
            $photo->setProduct($product);

            $photoRepository->add($photo, $limit, true);

            return $this->returnPhoto($photo);
        } catch (Error|UniqueConstraintViolationException $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '/{id}', name: 'patch', methods: ['PATCH'])]
    public function patch(Request $request, PhotoRepository $photoRepository, ProductRepository $productRepository): JsonResponse
    {
        try {
            $photo = $photoRepository->findOneById($this->getIdFromRoute($request));
            $product = null;

            $idProduct = $request->request->get('idProduct');

            if ($idProduct) {
                $product = $productRepository->findOneById($idProduct);
            }

            $filename = $request->request->get('filename');
            $photoRepository->patch($photo, $filename, $product, true);

            return $this->returnPhoto($photo);
        } catch (\Error $error) {
            return $this->returnError($error);
        }
    }

    #[Route(path: '/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(Request $request, PhotoRepository $photoRepository): Response | JsonResponse
    {
        try {
            $photo = $photoRepository->findOneById($this->getIdFromRoute($request));

            $photoRepository->delete($photo, true);

            return $this->returnNoContent();
        } catch (\Error $error) {
            return $this->returnError($error);
        }
    }

    private function returnPhoto(Photo $photo): JsonResponse
    {
        return $this->json([
            'photo' => $this->formatResponsePhoto($photo)
        ]);
    }
}
