<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Photo;
use App\Entity\Product;
use App\ResponseEntity\CategoryResponse;
use App\ResponseEntity\PhotoOnlyResponse;
use App\ResponseEntity\PhotoResponse;
use App\ResponseEntity\ProductOnlyResponse;
use App\ResponseEntity\ProductResponse;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Error;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    protected function formatResponseCategory(Category $category): CategoryResponse
    {
        return new CategoryResponse(
            id: $category->getId(),
            uuid: $category->getUuid(),
            name: $category->getName(),
        );
    }

    protected function formatResponseProduct(Product $product): ProductResponse
    {
        return new ProductResponse(
            id: $product->getId(),
            uuid: $product->getUuid(),
            name: $product->getName(),
            category: $this->formatResponseCategory($product->getCategory()),
            photos: $this->formatResponsePhotosOnly($product->getPhotos())
        );
    }

    protected function formatResponsePhoto(Photo $photo): PhotoResponse
    {
        return new PhotoResponse(
            id: $photo->getId(),
            uuid: $photo->getUuid(),
            filename: $photo->getFilename(),
            product: $this->formatResponseProductOnly($photo->getProduct())
        );
    }

    protected function getIdFromRoute(Request $request): int
    {
        $routeParams = $request->attributes->get('_route_params');
        $id = (int)$routeParams['id'];

        if (!$id) {
            throw new Error('ID not found', 222);
        }

        return $id;
    }

    protected function returnError(Error|UniqueConstraintViolationException $error): JsonResponse
    {
        return $this->json([
            'error' => $error->getMessage(),
            'code' => $error->getCode()
        ], 404);
    }

    protected function returnNoContent(): Response
    {
        return new Response('', Response::HTTP_NO_CONTENT);
    }

    private function formatResponseProductOnly(Product $product)
    {
        return new ProductOnlyResponse(
            id: $product->getId(),
            uuid: $product->getUuid(),
            name: $product->getName()
        );
    }

    private function formatResponsePhotosOnly(Collection $photos)
    {
        if (!$photos) {
            return [];
        }

        $photosResponse = [];

        foreach ($photos as $photo) {
            $photosResponse[] = new PhotoOnlyResponse(
                id: $photo->getId(),
                uuid: $photo->getUuid(),
                filename: $photo->getFilename()
            );
        }

        return $photosResponse;
    }
}
