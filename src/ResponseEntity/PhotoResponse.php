<?php

namespace App\ResponseEntity;

class PhotoResponse {

    public function __construct(
        public readonly int $id,
        public readonly string $uuid,
        public readonly string $filename,
        public readonly ProductOnlyResponse $product
    )
    {

    }
}
