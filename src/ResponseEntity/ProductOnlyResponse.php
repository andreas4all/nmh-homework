<?php

namespace App\ResponseEntity;

use Doctrine\Common\Collections\Collection;

class ProductOnlyResponse {

    public function __construct(
        public readonly int $id,
        public readonly string $uuid,
        public readonly string $name,
    )
    {

    }
}
