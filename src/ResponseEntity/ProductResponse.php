<?php

namespace App\ResponseEntity;

use Doctrine\Common\Collections\Collection;

class ProductResponse {

    public function __construct(
        public readonly int $id,
        public readonly string $uuid,
        public readonly string $name,
        public readonly CategoryResponse $category,
        public readonly array $photos
    )
    {

    }
}
