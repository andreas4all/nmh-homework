<?php

namespace App\ResponseEntity;

class CategoryResponse {

    public function __construct(
        public readonly int $id,
        public readonly string $uuid,
        public readonly string $name
    )
    {

    }
}
