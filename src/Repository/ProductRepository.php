<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Error;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findOneById(int $id):? Product
    {
        $product = $this->find($id);

        if (!$product) {
            throw new Error('Product not found', 131);
        }

        return $product;
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $entity->setUuid(Uuid::v4());
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function patch(Product $entity, string $name = null, Category $category = null, bool $flush = false): void
    {
        if ($name) {
            $entity->setName($name);
        }

        if ($category) {
            $entity->setCategory($category);
        }

        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function delete(Product $entity, bool $flush = false): void
    {
        $photos = $entity->getPhotos();

        if (!$photos->isEmpty()){
            throw new Error('Product has photos, please delete it first!', 132);
        }

        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
