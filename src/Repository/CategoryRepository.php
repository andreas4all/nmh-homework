<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Error;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Category>
 *
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findOneById(int $id):? Category
    {
        $category = $this->find($id);

        if (!$category) {
            throw new Error(sprintf('Category ID %d not found!', $id), 111);
        }

        return $category;
    }

    public function add(Category $entity, bool $flush = false): void
    {
        $entity->setUuid(Uuid::v4());
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function patch(Category $entity, string $name, bool $flush = false): void
    {
        $entity->setName($name);
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function delete(Category $entity, bool $flush = false): void
    {
        $products = $entity->getProducts();

        if (!$products->isEmpty()){
            throw new Error('Category has products, please delete it first!', 132);
        }

        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
