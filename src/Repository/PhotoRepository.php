<?php

namespace App\Repository;

use App\Entity\Photo;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Error;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Photo>
 *
 * @method Photo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Photo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Photo[]    findAll()
 * @method Photo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Photo::class);
    }

    public function findOneById(int $id):? Photo
    {
        $photo = $this->find($id);

        if (!$photo) {
            throw new Error('Photo not found', 121);
        }

        return $photo;
    }

    public function add(Photo $entity, int $limit, bool $flush = false): void
    {
        $product = $entity->getProduct();
        $photos = $product->getPhotos();

        if (count($photos) >= $limit) {
            throw new Error('Too many photos on one product', 122);
        }

        $entity->setUuid(Uuid::v4());
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function patch(Photo $entity, string $filename = null, Product $product = null, bool $flush = false): void
    {
        if ($filename) {
            $entity->setFilename($filename);
        }

        if ($product) {
            $entity->setProduct($product);
        }

        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function delete(Photo $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
