### NMH Homework

## Install

Install CA for server
```bash
symfony server:ca:install
```

## Database setup

Copy _.env.dist_ to _.env_ and edit it.


```bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

## Start server

Start web server with
```bash
symfony server:start
```
